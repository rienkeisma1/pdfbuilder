<?php
namespace AppBundle\Services;


use Doctrine\ORM\EntityManager;
use AppBundle\Entity\PdfData;

use Doctrine\ORM\Tools\Pagination\Paginator;

class PdfCrud
{

    public $em;
    
    public function __construct(EntityManager $entityManager) {

        $this->em = $entityManager;

    }

    /**
     * Creates a new PdfData row.
     *
     * @param array $input
     * @param user  $user
     *
     * @return array error or success
     */

    public function createPdfData($input, $user)
    {
        //todo isset/ternary etc

        //check if exists
        $repository = $this->em
        ->getRepository('AppBundle:PdfData');

        $existing = $repository->findOneBy(['created' => $input['created'], 'userpdfs' => $user]);

        if($existing !== null){

            return $this->updatePdfData($input, $user, $existing);

        }else{

            $pdf = new PdfData();
            $pdf->setName($input['name']);
            $pdf->setBorder($input['border']);
            $pdf->setHead($input['headval']);
            $pdf->setBody($input['bodyval']);
            $pdf->setJavascript($input['jsval']);
            $pdf->setCss($input['cssval']);
            $pdf->setHeader($input['header']['html']);
            $pdf->setFooter($input['footer']['html']);
            $pdf->setShowheader($input['header']['showheader']);
            $pdf->setHeaderheight($input['header']['height']);
            $pdf->setShowfooter($input['footer']['showfooter']);
            $pdf->setFooterheight($input['footer']['height']);
            $pdf->setLastupdate($input['lastupdate']);
            $pdf->setCreated($input['created']);
            $pdf->setDeleted(false);
            $pdf->setUserpdfs($user);

            $this->em->persist($pdf);

            try{

                $this->em->flush();

                return ['msg' => ['success' => 'saved pdf', ], 'code' => 200];

            } catch(\Doctrine\ORM\ORMException $e){

                //TODO log errors $e->getMessage()
                return ['msg' => ['error' => 'error saving pdf'], 'code' => 500];


            } catch(\Exception $e){

                //TODO log errors $e->getMessage()
                return ['msg' => ['error' => 'error saving pdf'], 'code' => 500];

            }

        }

    }

    /**
     * Updates all pdfdata and returns outdated to client.
     * IMPORTANT: Not used right now because:
     * -needs advanced frontend handling &
     * -returns  "The EntityManager is closed" (sometimes) -->do research 
     * @param array $input
     * @param user  $user
     *
     * @return array : workload for client: create, update, delete
     */
    public function updateAllPdfData($input, $user)
    {

        $comparearray = [];
        $fullarray = [];

        dump($input);

        foreach($input as $v){

            $comparearray[$v['created']] = $v['lastupdate'];

            $fullarray[$v['created']] = $v;

        }

        //now find them all
        $repository = $this->em
        ->getRepository('AppBundle:PdfData');

        $res = $repository->findBy(['userpdfs' => $user]);

        //result is a tasks array for the client
        $result = [];
        $result['update'] = [];
        $result['deleted'] = [];
        $result['create'] = [];
        //info on server updated
        $result['serverupdated'] = [];
        $result['servercreated'] = [];

        $workload = [];
        $serverupdated = [];
        $servercreated = [];

        foreach($res as $k){

            if( isset($comparearray[$k->getCreated()]) ){

                //exists on client
                if($k->getDeleted() === true){

                    //$result['deleted'][] = $this->createArray($k);
                    $t = $this->createArray($k);
                    $t['type'] = 'delete';
                    $workload[] = $t;

                }else if($k->getLastupdate() > $comparearray[$k->getCreated()]){
                    //server newer
                    //$result['update'] = $this->createArray($k);
                    $t = $this->createArray($k);
                    $t['type'] = 'update';
                    $workload[] = $t;

                }else if($k->getLastupdate() < $comparearray[$k->getCreated()]){
                    //client newer
                    $this->updatePdfData($fullarray[$k->getCreated()], $user, $k);
                    $serverupdated[] = $fullarray[$k->getCreated()];
                    
                }

                unset($comparearray[$k->getCreated()]);

            }else{

                //not set on client
                if($k->getDeleted() !== true){

                    $t = $this->createArray($k);
                    $t['type'] = 'create';
                    $workload[] = $t;

                }

            }

        }

        //now push to the database which are remaining
        foreach($comparearray as $k => $v){

            $this->createPdfData($fullarray[$k], $user);
            $servercreated[] = $fullarray[$k];

        }

        return  ['workload'=> $workload, 'servercreated' => $servercreated, 'serverupdated' => $serverupdated];

    }

    /**
     * object to array to return to the client
     *
     * @param object $input
     *
     * @return array
     */
    private function createArray($pdf)
    {

        $arr = [];

        $arr['name'] = $pdf->getName();
        $arr['border'] = $pdf->getBorder();
        $arr['headval'] = $pdf->getHead();
        $arr['bodyval'] = $pdf->getBody();
        $arr['jsval'] = $pdf->getJavascript();
        $arr['cssval'] = $pdf->getCss();
        $arr['header'] = [];
        $arr['header']['html'] = $pdf->getHeader();
        $arr['header']['height'] = $pdf->getHeaderheight();
        $arr['header']['showheader'] = $pdf->getShowheader();
        $arr['footer'] = [];
        $arr['footer']['html'] = $pdf->getFooter();
        $arr['footer']['height'] = $pdf->getFooterheight();
        $arr['footer']['showfooter'] = $pdf->getShowfooter();
        $arr['lastupdate'] = $pdf->getLastupdate();
        $arr['created'] = $pdf->getCreated();

        return $arr;    

    }


    /**
     * Updates a PdfData row.
     *
     * @param array $input
     * @param user  $user
     *
     * @return array error or success
     */
    public function updatePdfData($input, $user, $repo = false)
    {
        //todo isset/ternary etc

        if($repo !== false && $repo !== null){

            $pdf = $repo;

        }else{

            $repository = $this->em
            ->getRepository('AppBundle:PdfData');

            $pdf = $repository->findOneBy(['userpdfs' => $user, 'created' => $input['created']]);

        }

        //avoid deleted
        if(null !== $pdf->getDeleted() && $pdf->getDeleted() === true){

            //early return
            return ['msg' => ['error' => 'pdf deleted'], 'code' => 409];

        }

        $pdf->setName($input['name']);
        $pdf->setBorder($input['border']);
        $pdf->setHead($input['headval']);
        $pdf->setBody($input['bodyval']);
        $pdf->setJavascript($input['jsval']);
        $pdf->setCss($input['cssval']);
        $pdf->setHeader($input['header']['html']);
        $pdf->setFooter($input['footer']['html']);
        $pdf->setShowheader($input['header']['showheader']);
        $pdf->setHeaderheight($input['header']['height']);
        $pdf->setShowfooter($input['footer']['showfooter']);
        $pdf->setFooterheight($input['footer']['height']);
        $pdf->setLastupdate($input['lastupdate']);
        $pdf->setCreated($input['created']);
        $pdf->setUserpdfs($user);

        
        try{

            $this->em->flush();
            
            return ['msg' => ['success' => 'updated pdf'], 'code' => 200];

        } catch(\Doctrine\ORM\ORMException $e){

            //TODO log errors $e->getMessage()
            return ['msg' => ['error' => 'error updating pdf'], 'code' => 500];


        } catch(\Exception $e){

            //TODO log errors $e->getMessage()
            return ['msg' => ['error' => 'error updating pdf'], 'code' => 500];

        }

    }

    /**
     * Deletes a PdfData row.
     *
     * @param array $input
     * @param user  $user
     *
     * @return array error or success
     */
    public function deletePdf($created, $user)
    {
        //todo isset/ternary etc

        $repository = $this->em
        ->getRepository('AppBundle:PdfData');

        $pdf = $repository->findOneBy(['userpdfs' => $user, 'created' => $created]);

        if($pdf === null){

            return ['msg' => ['error' => 'could not find this pdf'], 'code' => 404];

        }

        $pdf->setName(null);
        $pdf->setBorder(null);
        $pdf->setHead(null);
        $pdf->setBody(null);
        $pdf->setJavascript(null);
        $pdf->setCss(null);
        $pdf->setHeader(null);
        $pdf->setFooter(null);
        $pdf->setShowheader(null);
        $pdf->setHeaderheight(null);
        $pdf->setShowfooter(null);
        $pdf->setFooterheight(null);
        $pdf->setLastupdate(null);
        $pdf->setDeleted(true);
        
        try{

            $this->em->flush();
           
            return ['msg' => ['success' => 'deleted pdf'], 'code' => 200];

        } catch(\Doctrine\ORM\ORMException $e){

            //TODO log errors $e->getMessage()
            return ['msg' => ['error' => 'error deleting pdf'], 'code' => 500];


        } catch(\Exception $e){

            //TODO log errors $e->getMessage()
            return ['msg' => ['error' => 'error deleting pdf'], 'code' => 500];

        }

    }

    /**
     * Gets a Pdfs metadata by User. For fine-graned sync logic and list creation(ie low-traffic).
     *
     * @param user  $user
     *
     * @return array of pdf metadata objects
     */
    public function getPdfsMetaDataByUser($user)
    {

        $repository = $this->em
        ->getRepository('AppBundle:PdfData');

        $pdfs = $repository->findBy(['userpdfs' => $user, 'deleted' => false]);

        $metadata = [];

        foreach($pdfs as $v) {

            $metadata[] = ['name' => $v->getName(), 'created' => $v->getCreated(), 'lastupdate' => $v->getLastUpdate()];

        }

        return ['data' => $metadata, 'code' => 200];

    }

    /**
     * Gets a Pdfs by User.
     * Not used right now: metadata is for listdata
     * @param user  $user
     *
     * @return array of pdf objects
     */
    public function getPdfsByUser($user)
    {

        $repository = $this->em
        ->getRepository('AppBundle:PdfData');

        $pdfs = $repository->findBy(['userpdfs' => $user, 'deleted' => false]);
       
        return $pdfs;

    }

    /**
     * Gets a single Pdf by created key and user.
     *
     * @param user  $user
     * @param string  $created
     *
     * @return pdf object
     */
    public function getPdf($created, $user)
    {

        $repository = $this->em
        ->getRepository('AppBundle:PdfData');

        $pdf = $repository->findOneBy(['userpdfs' => $user, 'created' => $created]);

        if($pdf === null){

            return ['msg' => ['error' => 'could not find this pdf'], 'code' => 404];

        }

        if($pdf->getDeleted() === true){

            return ['msg' => ['error' => 'could not find this pdf'], 'code' => 404];

        }

        return ['data' => $this->createArray($pdf), 'code' => 200];

       

    }

}