<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiControllerTest extends WebTestCase
{
    public function testCreatepdfdata()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/createPdfData');
    }

    public function testUpdatepdfdata()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/updatePdfData');
    }

    public function testDeletepdfdata()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/deletePdfData');
    }

    public function testGetpdfs()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/getPdfs');
    }

    public function testGetpdf()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/getPdf');
    }

}
