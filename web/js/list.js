var listhandler = {};

listhandler.settled = false;
listhandler.settledlocal = false;
listhandler.settledserver = false;
listhandler.servermetadata = false;

listhandler.createLocal = function(callback){

	let _this = this;

	localforage.getItem('builds').then(function(res){

		if(res !== null){

			let el = $('#localprojectslist');

			$(el).empty();

			for(let i = 0; i < res.length; i++) {

				if(res[i].name.trim() === ''){

					res[i].name = 'no name supplied!';
					
				}

				$(el).append('<div class="list-group-item"><a href="' + baseurl + config.urlprepend + '/edit/'+res[i].created+'" >'+res[i].name+'</a><button data-id="'+res[i].created+'" type="button" class="updatecloudbutton optionhidden btn btn-primary pull-right">Update cloud</button><button data-id="'+res[i].created+'" type="button" class="addtocloudbutton optionhidden btn btn-primary pull-right">Add to cloud</button><button data-id="'+res[i].created+'" type="button" class="listdeletebutton btn btn-danger pull-right">Delete</button></div>');

			}

			_this.createLocalListListeners();

		}

		if(_this.settledserver === false || _this.settledlocal === false){

			_this.setSettled('local');

		}else{

			_this.handleDiffs();
		}

		callback('ok');

	}).catch(function(err){

		$('#msgbox').empty().html(JSON.stringify(err));

	})

}

listhandler.createLocalListListeners = function(){

	let _this = this;

	//delete handler
	$(document).find('button.listdeletebutton').on('click', function(){

		_this.deleteItem($(this).data('id'));

	})

}

$(function() {

	listhandler.createLocal();

	

	listhandler.handleCloud();

	

});


listhandler.deleteItem = function(created) {

	let _this = this;

	localforage.getItem('builds').then(function(res){

		for (let i = 0; i < res.length; i++){

			if(parseInt(res[i].created) === parseInt(created)){

				res.splice(i, 1);
				break;

			}

		}

		return localforage.setItem('builds', res);

	}).then(function(res){

		_this.createLocal();

	}).catch(function(err){

		$('#msgbox').empty().html(JSON.stringify(err));

	})

}

listhandler.handleCloud = function() {

	if (config.cloudsupport === false){

		$('form#registerform').hide();
		$('form#loginform').hide();
		$('div#logoutholder').hide();
		$('div#cloudprojectslist').hide();
		$('button#syncprojects').hide();
		$('div#cloudlistholder').hide();
		$('.cloudrelated').hide();

		return;

	}

	let _this = this;

	if(typeof auth === 'undefined') {

		$('#msgbox').empty().html('missing script: "authorize.js", please contact the owner');
	
	}else{

		auth.authorize(function(res){

			$('#loader').css('display', 'none');

			if(res === 'showforms'){

				$('form#registerform').show();
				$('form#loginform').show();

			}else if (res === 'success'){

				$('div#logoutholder').show();

				_this.createCloud();

			}else{

				//error
				$('form#registerform').show();
				$('form#loginform').show();
				$('div#logoutholder').hide();
				$('div#cloudprojectslist').empty();

				$('#msgbox').empty().html(JSON.stringify(res));

			}

		});

	}

}

listhandler.createCloud = function(){

	let _this = this;

	//get items
	crud.getProjectsMetaData(function(resp){

		if(typeof resp.data !== 'undefined'){

			_this.servermetadata = resp.data;

			let el = $('#cloudprojectslist');

			$(el).empty();

			let res = resp.data;

			for(let i = 0; i < res.length; i++) {

				//$(el).append('<div class="list-group-item"><a href="'+baseurl+urlprepend+'/edit/'+res[i].created+'" >'+res[i].name+'</a><button data-id="'+res[i].created+'" type="button" class="updatelocalbutton optionhidden btn btn-primary pull-right">Update local</button><button data-id="'+res[i].created+'" type="button" class="addtolocalbutton optionhidden btn btn-primary pull-right">Add to local</button><button data-id="'+res[i].created+'" type="button" class="cloudlistdeletebutton btn btn-danger pull-right">Delete</button></div>');
				$(el).append('<div class="list-group-item"><span>'+res[i].name+'</span><button data-id="'+res[i].created+'" type="button" class="updatelocalbutton optionhidden btn btn-primary pull-right">Update local</button><button data-id="'+res[i].created+'" type="button" class="addtolocalbutton optionhidden btn btn-primary pull-right">Add to local</button><button data-id="'+res[i].created+'" type="button" class="cloudlistdeletebutton btn btn-danger pull-right">Delete</button></div>');


			}

			_this.createCloudListListeners();
			_this.setSettled('server');

		}else{

			_this.setSettled('server');
		}

		_this.handleDiffs();

	})

}

listhandler.createCloudListListeners = function(){

	let _this = this;

	//delete handler
	$(document).find('button.cloudlistdeletebutton').on('click', function(){

		_this.deleteCloudItem($(this).data('id'));

	})

}

listhandler.deleteCloudItem = function(created) {

	let _this = this;

	crud.deleteProject(created, function(resp){

		//refresh list
		_this.createCloud();

	})

}

listhandler.setSettled = function(type){

	let _this = this;

	_this['settled'+type] = true;

	if(_this.settledserver ===  true && _this.settledlocal === true && _this.servermetadata !== false && _this.settled === false) {

		_this.settled = true;

		_this.handleDiffs();

	}

}

listhandler.handleDiffs = function() {

	let _this = this;

	localforage.getItem('builds').then(function(res){

		let server = {};

		for(let i = 0; i < _this.servermetadata.length; i++) {

			server[parseInt(_this.servermetadata[i].created)] = _this.servermetadata[i];

		}

		for(let i = 0; i < res.length; i++) {

			if(typeof server[parseInt(res[i].created)] === 'undefined'){

				$(document).find('button.addtocloudbutton[data-id="' + res[i].created + '"]').removeClass('optionhidden');

			}else if(typeof server[parseInt(res[i].created)] !== 'undefined' && parseInt(server[parseInt(res[i].created)].lastupdate) >= parseInt(res[i].lastupdate)){

				$(document).find('button.addtocloudbutton[data-id="' + res[i].created + '"]').addClass('optionhidden');

			}else if(parseInt(server[parseInt(res[i].created)].lastupdate) < parseInt(res[i].lastupdate)){

				$(document).find('button.updatecloudbutton[data-id="' + res[i].created + '"]').removeClass('optionhidden');

			}else if(parseInt(server[parseInt(res[i].created)].lastupdate) >= parseInt(res[i].lastupdate)){

				$(document).find('button.updatecloudbutton[data-id="' + res[i].created + '"]').addClass('optionhidden');

			}

		}

		_this.createCloudUpdateListeners();

		let local = {};

		for(let i = 0; i < res.length; i++) {

			local[parseInt(res[i].created)] = res[i];

		}

		for(let i = 0; i < _this.servermetadata.length; i++) {

			if(typeof local[parseInt(_this.servermetadata[i].created)] === 'undefined'){

				$(document).find('button.addtolocalbutton[data-id="' + _this.servermetadata[i].created + '"]').removeClass('optionhidden');

			}else if(typeof local[parseInt(_this.servermetadata[i].created)] !== 'undefined' && parseInt(local[parseInt(_this.servermetadata[i].created)].lastupdate) >= parseInt(_this.servermetadata[i].lastupdate)){

				$(document).find('button.addtolocalbutton[data-id="' + _this.servermetadata[i].created + '"]').addClass('optionhidden');

			}else if(parseInt(local[parseInt(_this.servermetadata[i].created)].lastupdate) < parseInt(_this.servermetadata[i].lastupdate)){

				$(document).find('button.updatelocalbutton[data-id="' + _this.servermetadata[i].created + '"]').removeClass('optionhidden');

			}else if(parseInt(local[parseInt(_this.servermetadata[i].created)].lastupdate) >= parseInt(_this.servermetadata[i].lastupdate)){

				$(document).find('button.updatelocalbutton[data-id="' + _this.servermetadata[i].created + '"]').addClass('optionhidden');

			}

		}

		_this.createLocalUpdateListeners();

	})

}

listhandler.createCloudUpdateListeners = function(){

	let _this = this;

	$(document).find('button.addtocloudbutton').off().on('click', function(){

		_this.addItemToCloud($(this).data('id'));

	})

	$(document).find('button.updatecloudbutton').off().on('click', function(){

		_this.updateItemToCloud($(this).data('id'));

	})

}

listhandler.createLocalUpdateListeners = function() {

	let _this = this;

	$(document).find('button.addtolocalbutton').off().on('click', function(){

		_this.addItemToLocal($(this).data('id'));

	})

	$(document).find('button.updatelocalbutton').off().on('click', function(){

		_this.updateItemToLocal($(this).data('id'));

	})

}

listhandler.addItemToCloud = function(created) {

	let _this = this;

	crud.createProject(created, function(resp){

		_this.createCloud();

	}, function(err){

		$('#msgbox').empty().html('error: '+JSON.stringify(err));

	});

}


listhandler.updateItemToCloud = function(created) {

	let _this = this;

	crud.updateProject(created, function(resp){

		_this.settledlocal = false;
		_this.settledserver = false

		_this.createLocal(function(res){

			_this.createCloud();

		});

	}, function(err){

		$('#msgbox').empty().html('error: '+JSON.stringify(err));

	});
	
}

listhandler.addItemToLocal = function(created) {

	let _this = this;

	crud.getProject(created, function(resp){

		localforage.getItem('builds').then(function(res){

			resp.data.trusted = false;

			res.unshift(resp.data);

			return localforage.setItem('builds', res);

		}).then(function(res){

			_this.createLocal();
			_this.handleDiffs();

		}).catch(function(err){

			$('#msgbox').empty().html('error: '+JSON.stringify(err));

		})

	}, function(err) {

		$('#msgbox').empty().html('error: '+JSON.stringify(err));

	});

}

listhandler.updateItemToLocal = function(created) {

	let _this = this;

	crud.getProject(created, function(resp){

		localforage.getItem('builds').then(function(res){

			resp.data.trusted = false;

			res.unshift(resp.data);

			for(let i = 0; i < res.length; i++) {

				if(parseInt(res[i].created === resp.data.created)){

					res[i] = resp.data;
					break;
				
				}

			}

			return localforage.setItem('builds', res);

		}).then(function(res){

			_this.createLocal();
			_this.handleDiffs();

		}).catch(function(err){

			$('#msgbox').empty().html('error: '+JSON.stringify(err));

		})


	}, function(err) {

		$('#msgbox').empty().html('error: '+JSON.stringify(err));

	});

}

