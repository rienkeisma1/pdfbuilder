var auth = {};
auth.authorized = false;

auth.authorize = function(callback){

	//TRY TO LOGIN ON LOAD
	localforage.getItem('jwttoken').then(function(res){

		let succes = false;

		if(res === null){

			//no token
			callback('showforms');

		}else{

			auth.loginCheck(function(resp){

				if(resp === false){

					//todo, handle this logic
					auth.refreshtoken(function(refrres){

						callback(refrres);

					});

				}else{

					auth.authorized = true;

					callback('success');

				}

			});

		}

	}).catch(function(err){

		$('#msgbox').empty().html('an error occurred: '+JSON.stringify(err));

	})

}

auth.loginCheck = function(callback) {

	auth.call(baseurl + config.urlprepend + '/api/login_check', {}, 'POST', function(res){

		if(res.msg === 'success'){

			callback(true);
	
		}else{

			callback(false);
		}

	})

}

auth.call = function(url, data, type, callback) {

	localforage.getItem('jwttoken').then(function(token){

		//returning works as a promise !

		return $.ajax({
	        	
	        url: url,
	     	dataType: 'JSON',
	     	type: type,
	     	data: data,
	     	beforeSend: function(xhr) {
	            xhr.setRequestHeader("Authorization", "Bearer " + token);
	        },
	      	success: function(data){

	      	
	      	},
	     
	     	error: function(jqXHR, textStatus, errorThrown)   {

	        }

	    })

	}).then(function(res){

		callback(res);

	}).catch(function(err){

		$('#msgbox').html('an error occurred: '+JSON.stringify(err));

		callback({err: err});

	})


}


//TODO NOT VERY DRY REFACTOR

auth.register = function(email, password, callback, errcallback){

	$.post( baseurl + config.urlprepend + '/api/register', {email:email, password:password})
				  
  	.done(function( response ) {

  		//set token
  		localforage.setItem('jwttoken', response.token).then(function(res){

    		return localforage.setItem('jwtrefreshtoken', response.refresh_token);

    	}).then(function(res){

			callback({msg: 'you registered !'});

    	}).catch(function(err){

    		errcallback({err: err});
    	})

  	}).fail(function(err){

  		errcallback({err: err});

  	});

}

auth.refreshtoken = function(callback){

	$('#loader').css('display', 'block');

	//hack
	let error = false;

	var refreshtoken = '';

	localforage.getItem('jwtrefreshtoken').then(function(res){

		refreshtoken = res;

		return localforage.getItem('jwttoken');

	}).then(function(token){

		$.post( baseurl + config.urlprepend + '/api/refresh_token', {token:token, refresh_token: refreshtoken})

		.done(function( response ) {

			refreshtoken = response.refresh_token;

			return localforage.setItem('jwttoken', response.token);

		}).fail(function(err){

			error = true;

	  		callback({err: err});

  		});

  	}).then(function(res){

  		if(error === false){

  			return localforage.setItem('jwtrefreshtoken', refreshtoken);

  		}

	}).catch(function(err){

    	callback(err);

	}).then(function(success){

		if(error === false){

			callback(true);

		}

	})

}

auth.login = function(email, password, callback){

	$.post( baseurl + config.urlprepend + '/api/login', {email:email, password:password})
				  
  	.done(function( response ) {
  		//set token
  		localforage.setItem('jwttoken', response.token).then(function(res){

    		return localforage.setItem('jwtrefreshtoken', response.refresh_token);

    	}).then(function(res){

			callback({msg: 'loggedin'});

    	}).catch(function(err){

    		callback({err: err});
    	})
    		  
  	}).fail(function(err){

		callback({err: err});

  	});

}

auth.logout = function(callback) {

	localforage.setItem('jwtrefreshtoken', '').then(function(res){

		return localforage.setItem('jwttoken', '');

	}).then(function(res){

		callback({msg: 'loggedout'});

	}).catch(function(err){

		callback({err: err})

	})

}