<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

//phantomjs
use H2P\Converter\PhantomJS;
use H2P\TempFile;


class ApiController extends Controller
{


    /**
     * @param void
     * @return array
     * @Route("/api/login_check", name="login_check")
     * @Method({"POST"})
     */
    public function logincheckAction()
    {

        $response = new JsonResponse();
        $response->setData(['msg' => 'success']);

        return $response;

    }

    /**
     * @param postdata, see required keys below (header and footer data is nested)
     * @return array  ['link' => (string) link]
     * @Route("/api/createpdf", name="createpdf")
     * @Method({"POST"})
     */
    public function createpdfAction(Request $request)
    {

        $pdfdata = $request->request->get('data');
        $headercontent = $pdfdata['header']['html'];
        $footercontent = $pdfdata['footer']['html'];
        $border = $pdfdata['border'];
        $name = $pdfdata['name'];

        //uncomment to check/debug if phantomjs is available. When ftp'ed phantomjs can get corrupted, wget it from the server then(learned that the hard way)
        //$version =  shell_exec(realpath($this->getParameter('kernel.root_dir').'/..').'/phantomjs/phantomjs -v');
        //dump($version); (or var_dump if you don't use the symfony var dumper)

        try{
       
            $path = realpath($this->getParameter('kernel.root_dir').'/..').'/phantomjs/phantomjs';

            $params = [

                'search_paths' => $path,
                'orientation' => PhantomJS::ORIENTATION_PORTRAIT,
                'format' => PhantomJS::FORMAT_A4,
                'zoomFactor' => 0.85,
                'border' => $border
              
            ];


            dump($pdfdata['header']);
            if($pdfdata['header']['showheader'] === 'true' || $pdfdata['header']['showheader'] === true){

                $params['header']['height'] = $pdfdata['header']['height'];
                $params['header']['content'] = $pdfdata['header']['html'];

            }

            if($pdfdata['footer']['showfooter'] === 'true' || $pdfdata['footer']['showfooter'] === true){

                $params['footer']['height'] = $pdfdata['footer']['height'];
                $params['footer']['content'] = $pdfdata['footer']['html'];

            }

            $converter = new PhantomJS($params);
            
            $input = new TempFile($pdfdata['html'], 'html');

            $converter->convert($input, $_SERVER['DOCUMENT_ROOT'].'/downloads/'.$name.'.pdf');

            chmod($_SERVER['DOCUMENT_ROOT'].'/downloads/'.$name.'.pdf', 0777);
            
            $res = ['link' => '/downloads/'.$name.'.pdf', 'code' => 200];
       
        
        }catch (Exception $e){

            $res = ['error' => json_encode($e), 'code' => 500];

        }

        $response = new JsonResponse();
        $response->setStatusCode($res['code']);
        $response->setData($res);

        return $response;

    }

    /**
     * creates a new row or updates if exists
     * @param postdata 
     * @return array
     * @Route("/api/createPdfData")
     * @Method({"POST"})
     */
    public function createPdfDataAction(Request $request)
    {

        $data = $request->request->all();

        $user = $this->get('security.token_storage')->getToken()->getUser();
        
        $pdfservice = $this->get('app.PdfCrudHandler');

        $res = $pdfservice->createPdfData($data, $user);

        $response = new JsonResponse();
        $response->setStatusCode($res['code']);
        $response->setData($res['msg']);

        return $response;
       
    }

    /**
     * updates a row
     * @param postdata 
     * @return array
     * @Route("/api/updatePdfData")
     * @Method({"POST"})
     */
    public function updatePdfDataAction(Request $request)
    {

        $data = $request->request->all();

        $user = $this->get('security.token_storage')->getToken()->getUser();
        
        $pdfservice = $this->get('app.PdfCrudHandler');

        $res = $pdfservice->updatePdfData($data, $user);

        $response = new JsonResponse();
        $response->setStatusCode($res['code']);
        $response->setData($res);

        return $response;
       
    }

    /**
     * Deletes data except id and 'created' field. 
     * Sets 'deleted' flag to true (otherwise syncing with a local datastore would result in a delete-recreate loop with multiple clients)
     * @param postdata 
     * @return array
     * @Route("/api/deletePdf")
     * @Method({"POST"})
     */
    public function deletePdfDataAction(Request $request)
    {

        $data = $request->request->all();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $pdfservice = $this->get('app.PdfCrudHandler');

        $res = $pdfservice->deletePdf($data['created'], $user); 

        $response = new JsonResponse();
        $response->setStatusCode($res['code']);
        $response->setData(['data' => $res]);

        return $response;
       
    }


    /**
     * @param void 
     * @return array
     * @Route("/api/getPdfsMetaData")
     * @Method({"GET"})
     */
    public function getPdfsMetaDataAction(Request $request)
    {

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $pdfservice = $this->get('app.PdfCrudHandler');

        $res = $pdfservice->getPdfsMetaDataByUser($user);

        $response = new JsonResponse();
        $response->setStatusCode($res['code']);
        $response->setData($res);

        return $response;
      
    }

    /**
     * @param created timestamp 
     * @return array
     * @Route("/api/getPdf/{created}")
     * @Method({"GET"})
     */
    public function getPdfAction(Request $request, $created)
    {

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $pdfservice = $this->get('app.PdfCrudHandler');

        $res = $pdfservice->getPdf($created, $user); 

        $response = new JsonResponse();
        $response->setStatusCode($res['code']);
        $response->setData($res);

        return $response;
        
    }

}
