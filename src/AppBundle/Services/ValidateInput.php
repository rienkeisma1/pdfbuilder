<?php
namespace AppBundle\Services;


use Symfony\Component\Validator\Constraints as Assert;

/**
 * Validates input
 *
 * Validation of registration etc is handled by fosuserbundle 
 * This is input validation of pdf data
 * Comments out script and link tags
 */

class ValidateInput
{

    public $em;
    
    public function __construct(EntityManager $entityManager) {

        $this->em = $entityManager;

    }

    /**
     * Creates a new PdfData row.
     *
     * @param array $input
     * @param user  $user
     *
     * @return array error or success
     */

    public function createPdfData($input, $user)
    {
        //todo isset/ternary etc

        //check if exists
        $repository = $this->em
        ->getRepository('AppBundle:PdfData');

        $existing = $repository->findOneBy(['created' => $input['created'], 'pdfuser' => $user]);

        if($existing !== null){

            return $this->updatePdfData($input, $user, $existing);

        }else{

            $pdf = new PdfData();
            $pdf->setName($input['name']);
            $pdf->setBorder($input['border']);
            $pdf->setHead($input['headval']);
            $pdf->setBody($input['bodyval']);
            $pdf->setJavascript($input['jsval']);
            $pdf->setCss($input['cssval']);
            $pdf->setHeader($input['header']['html']);
            $pdf->setFooter($input['footer']['html']);
            $pdf->setShowheader($input['header']['showheader']);
            $pdf->setHeaderheight($input['header']['height']);
            $pdf->setShowfooter($input['footer']['showfooter']);
            $pdf->setFooterheight($input['footer']['height']);
            $pdf->setLastupdate($input['lastupdate']);
            $pdf->setCreated($input['created']);
            $pdf->setDeleted(false);
            $pdf->setPdfuser($user);

            $this->em->persist($pdf);

            try{

                $this->em->flush();

                return ['msg' => ['success' => 'saved pdf', ], 'code' => 200];

            } catch(\Doctrine\ORM\ORMException $e){

                //TODO log errors $e->getMessage()
                return ['msg' => ['error' => 'error saving pdf'], 'code' => 500];


            } catch(\Exception $e){

                //TODO log errors $e->getMessage()
                return ['msg' => ['error' => 'error saving pdf'], 'code' => 500];

            }

        }

    }

}