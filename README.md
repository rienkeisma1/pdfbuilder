#PDF builder (béta)

A project to create pdf's from html, javascript and css using PhantomJS.

Can store projects server side, create a backup file for a project and load that backup using the html5 File/FileList/FileReader API

Saves progress automatically 3 seconds after you stop typing.

Editors are ace.js editors.

Backend is symfony 3.1

### Install

run composer install and make sure the web/downloads directory is server writable. (sudo chown www-data -R web/downloads/ for ubuntu but your webserver may be something other then www-data) 

generate ssh key (see [RCHJWTUserBundle](https://github.com/chalasr/RCHJWTUserBundle) or [rLexikJWTAuthenticationBundle](https://github.com/lexik/LexikJWTAuthenticationBundle/blob/master/Resources/doc/index.md)). Don't forget to change the passphrase in parameters.yml (jwt_key_pass_phrase)! 

uncomment schemes in config/routing.yml to force https

Don't forget to add

```
RewriteEngine On
RewriteCond %{HTTP:Authorization} ^(.*)
RewriteRule .* - [e=HTTP_AUTHORIZATION:%1]
```
to your Virtual host.

Also run 
```
php bin/console rch:jwt:generate-keys
```
to generate keys (for development). See lexikjwt how to create them with a custom passphrase.

create a web/downloads folder (writable by server and readable by client)

#### Additional info

- ~~uses LexikJWTAuthenticationBundle, [read how to change or create your ssh key here](https://github.com/lexik/LexikJWTAuthenticationBundle/blob/master/Resources/doc/index.md).~~
- uses [RCHJWTUserBundle](https://github.com/chalasr/RCHJWTUserBundle) which depends on fosuserbundle and lexikJWTAuthenticationBundle.
- uses [h2p](https://github.com/kriansa/h2p) to generate pdf's.

### Tests

A TODO

#### Frontend

QUnit is there but the tests aren't there yet. Functions are outside Document.ready's so should be testable;
(run npm install)

#### Backend

Most is about interaction with the database/doctrine. Probably that means functional testing: [docs](http://symfony.com/doc/current/testing/doctrine.html)

created tests (run phpunit). Register/Login/Logout/Save/Update/Delete/RefuseUpdateDeleted/RefuseCreateDeleted

### Known bugs (todo)

~~Sometimes jquery doesn't get loaded.
wrapping the second try block in kriansa/h2p/bin/converter.js in:~~

~~```page.includeJs("https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js", function() { });```~~

~~seems to work.~~ 

Sometimes d3.js or other dynamic javascript doesn't show in the result. This is probably due to async loading. The onLoadFinished callback probably gets called before the async content is loaded.
Workaround: wrap the page.render in a timeout.

file: kriansa/h2p/bin/converter.js, line 114-122:

replace

```
page.render(options.destination, { format: 'pdf' });

console.log(JSON.stringify({
	success: true,
	response: null
}));

// Stop the script
phantom.exit(0);
``` 

with (you can increase the timeout if it still doesn't  work);

```
window.setTimeout(function () {
	page.render(options.destination, { format: 'pdf' });

	console.log(JSON.stringify({
		success: true,
		response: null
	}));

	// Stop the script
	phantom.exit(0);
}, 1000);
``` 


Bootstrap does not support multiple modals on one page (sometimes it seems to work but that's not enough).
- TODO: find a way to fix that. Workaround for now: refresh the page and the first modal will work.

### Other todos

-Have to add error callbacks to a few methods
-Create a nice error/success experience (JSON.stringify(err) can be improved);
-Styling
-Sync-all function


