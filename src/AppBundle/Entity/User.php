<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RCH\JWTUserBundle\Entity\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="PdfData", mappedBy="userpdfs")
     */
    private $pdfuser; 

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Add pdfuser
     *
     * @param \AppBundle\Entity\PdfData $pdfuser
     *
     * @return User
     */
    public function addPdfuser(\AppBundle\Entity\PdfData $pdfuser)
    {
        $this->pdfuser[] = $pdfuser;

        return $this;
    }

    /**
     * Remove pdfuser
     *
     * @param \AppBundle\Entity\PdfData $pdfuser
     */
    public function removePdfuser(\AppBundle\Entity\PdfData $pdfuser)
    {
        $this->pdfuser->removeElement($pdfuser);
    }

    /**
     * Get pdfuser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPdfuser()
    {
        return $this->pdfuser;
    }
}
