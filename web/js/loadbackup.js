var loadbackup = {};

$(function() {

  //not set through init ? why ?
  //TODO debug
  localforage.config({
    //autoselect driver// driver      : localforage.WEBSQL, // Force WebSQL; same as using setDriver()
    name        : 'pdfbuilder',
    version     : 1.0,
    //size        : 4980736, // Size of database, in bytes. WebSQL-only for now.
    storeName   : 'builds', // Should be alphanumeric, with underscores.
    description : 'stored builds'
  });

  loadbackup.addEventListeners();

})

loadbackup.addEventListeners = function(){

  let _this = this;

  document.getElementById('fileinput').addEventListener('change', _this.readFile, false);

  $('button#replaceexisting').on('click', function(res){

    _this.saveBackup(true);

  })

  $('button#loadbackup').on('click', function(res){

    _this.saveBackup();

  })

}

loadbackup.readFile = function(evt){

  console.log(this);

  let _this = loadbackup;

  let f = evt.target.files[0]; 

    if (f) {
      var r = new FileReader();
      r.onload = function(e) { 
        var contents = e.target.result;
        _this.downloadedobj = JSON.parse(contents);
        _this.validateBackup();
      }
      r.readAsText(f);

    } else { 

      $('#msgbox').empty().html("Failed to load file");
      
    }

}

loadbackup.downloadedobj = {};

loadbackup.validateBackup = function(){

  let obj = this.downloadedobj;

  let hit = false;

  localforage.getItem('builds').then(function(res){

    if(res === null){

      $('div#backupsegment').show();

    }else{

      for (let i = 0; i < res.length; i++){

        if(parseInt(res[i].created) === parseInt(obj.created)){

          $('div#loadbackupholder').hide();
          $('div#replaceexistingholder').show();

          hit = true;

          break;

        }

      }

      if(hit === false){

        $('div#replaceexistingholder').hide();
        $('div#loadbackupholder').show();

      }

    }

  }).catch(function(err){

    $('#msgbox').empty().html(JSON.stringify(err));

  })

}

loadbackup.saveBackup = function(replace){

  /*if(typeof replace === 'undefined'){

    replace = false;

  }*/

  let _this = this;

  typeof replace === 'undefined' ? replace = false: replace;
   
  //todo validate object

  localforage.getItem('builds').then(function(res){

    if(res === null){

      res = [];

    }

    if(replace === true){

      for(let i = 0; i < res.length; i++) {

        if(parseInt(res[i].created) === parseInt(_this.downloadedobj.created)){

          res[i] = _this.downloadedobj;
          res[i].trusted = false;

        }

      }

    }else{

      _this.downloadedobj.trusted = false;

      res.unshift(_this.downloadedobj);

    }

    return localforage.setItem('builds', res);

  }).then(function(res){

    //go to edit page

    window.location.href = baseurl + config.urlprepend+'/edit/'+_this.downloadedobj.created;

  }).catch(function(err){

    $('#msgbox').empty().html(JSON.stringify(err));

  })
    
}

  