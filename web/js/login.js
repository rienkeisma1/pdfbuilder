var formlogin = {};

$(function() {

	formlogin.addEventHandlers();
	
});

formlogin.addEventHandlers = function(){

	$('form#loginform').on('submit', function(e){

		e.preventDefault();

		$('#loader').css('display', 'block');

		auth.login($('input#loginEmail').val(), $('input#loginPassword').val(), function(res){

			console.log(res);

			$('#loader').css('display', 'none');

			if(typeof res.msg !== 'undefined'){

				$('#msgbox').html(res.msg);

				$('form#registerform').hide();
				$('form#loginform').hide();

				$('div#logoutholder').show();

				//loggedin now call get listdata

				listhandler.handleCloud();

			}else if(typeof res.err !== 'undefined'){

				$('#msgbox').html('an error occurred: '+JSON.stringify(res.err));

			}else{

				$('#msgbox').html('unknown response: '+JSON.stringify(err));
			}

		});

	})

}