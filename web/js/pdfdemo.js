var pdfdemodata = {};

pdfdemodata.name = 'DEMO';
pdfdemodata.headval = `
<meta charset="utf-8">
<link href="${baseurl}/css/bootstrap.min.css" rel="stylesheet" />
<script src='https://d3js.org/d3.v3.min.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
`;

pdfdemodata.bodyval = `
<div class='container'>
    <div class='row'>
        <div class='col-md-12'>
            <h1>test</h1>
        </div>
    </div>
    <div id="body"></div>
</div>
`;

pdfdemodata.cssval = `
h1 { color: blue}

body {
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  margin: auto;
  position: relative;
  width: 400px;
}

text {
  font: 10px sans-serif;
}

form {
  position: absolute;
  right: 10px;
  top: 10px;
}
`;

pdfdemodata.header = {};
pdfdemodata.header.height = '80px';
pdfdemodata.header.html = `
<div style='overflow:hidden;height:100%;margin-top:-10px;padding-bottom:20px;margin-left:-20px; margin-right:-20px;width:105%; border-bottom:1px solid grey; '>
	<div style='border-bottom:1px solid #bdbdbd;background-color:#eee;width: inherit;height:90%;margin-top:0;'>
		<p style='font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; text-align:center;padding-top: 20px;padding-bottom:color:#333;margin-bottom:60px;font-weight:lighter;'>
			My Header
		</p>
	</div>
</div>
`;
pdfdemodata.header.showheader = true;

pdfdemodata.footer = {};
pdfdemodata.footer.height = '50px';
pdfdemodata.footer.html = `
<div style='width: 105%; margin-left: -10px; margin-right: -10px; margin-bottom: -25px;margin-top:-2px; padding-top: 11px; padding-bottom:10px;'>
	<div style='border-top: 1px solid #bdbdbd;padding-top:0;background-color: #eee; width: 100%; height: 100%; top: 8px;'>
		<p style='margin-top:9px;margin-left:-10px;text-align:center;font-size:70%;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif; color:#333;font-weight:lighter;'>
			{{pageNum}} of {{totalPages}}
		</p>
	</div>
</div>
`;
pdfdemodata.footer.showfooter = true;

pdfdemodata.border = '0px';
pdfdemodata.created = Date.now();
pdfdemodata.lastupdate = Date.now();

pdfdemodata.jsval = `
$(function() {
    
var dataset = {

  apples: [53245, 28479, 19697, 24037, 40245]

};

var width = 410,
    height = 300,
    radius = Math.min(width, height) / 2;

var color = d3.scale.category20();

var pie = d3.layout.pie()
    .sort(null);

var arc = d3.svg.arc()
    .innerRadius(radius - 100)
    .outerRadius(radius - 50);

var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

var path = svg.selectAll("path")
    .data(pie(dataset.apples))
  	.enter().append("path")
    .attr("fill", function(d, i) { return color(i); })
    .attr("d", arc);

    
})`;


