var logout = {};

$(function() {

	logout.addEventHandlers();
	
});

logout.addEventHandlers = function(){

	$('button#logoutbutton').on('click', function(e){

		$('#loader').css('display', 'block');

		auth.logout(function(res){

			$('#loader').css('display', 'none');

			if(typeof res.msg !== 'undefined'){

				$('#msgbox').html(res.msg);

				$('form#registerform').show();
				$('form#loginform').show();

				$('div#logoutholder').hide();

				//loggedout now hide cloudlist

				$('div#cloudprojectslist').empty();

				listhandler.servermetadata = false;
				listhandler.createLocal();


			}else if(typeof res.err !== 'undefined'){

				$('#msgbox').html('an error occurred: '+JSON.stringify(res.err));

			}else{

				$('#msgbox').html('unknown response: '+JSON.stringify(res));

			}

		});

	})

}