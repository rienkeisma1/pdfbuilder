var backup = {};

backup.supported = false;

$(function() {

	backup.checksupport(function(success){

		backup.supported = true;

	}, function(err){

		$('#msgbox').empty().html(err);

	})
	
});


backup.checksupport = function(success, err){

	if (window.File && window.FileReader && window.FileList && window.Blob) {

	  success(true);

	} else {

	  err('The File APIs are not fully supported in this browser.');

	}

}


backup.createData = function(created, result){

	if (this.supported === false){

		$('#msgbox').empty().html('The File APIs are not fully supported in this browser.');
		return;

	};

	localforage.getItem('builds').then(function(res){

		let downloadstring = '';

		for (let i = 0; i < res.length; i++){

			if(parseInt(res[i].created) === parseInt(created)){

				let d = new Date();
				let time = d.toLocaleString();
				time.replace(" ", "");

				let name = res[i].name;

				name.trim().replace(" ", "_");

				name += '-'+time+'.pdfbuilder';

				let data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(res[i]));
				downloadstring = '<a class="btn btn-primary" href="data:' + data + '" download="'+name+'">download Backup</a>';

				break;

			}

		}

		if(downloadstring === ''){

			downloadstring = '<p>Project not found !</p>';

		}

		result(downloadstring);


	}).catch(function(err){

		$('#msgbox').empty().html(JSON.stringify(err));

	})

	
}

