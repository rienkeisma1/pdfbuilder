//set localforage
localforage.config({
    //autoselect driver// driver      : localforage.WEBSQL, // Force WebSQL; same as using setDriver()
    name        : 'pdfbuilder',
    version     : 1.0,
    //size        : 4980736, // Size of database, in bytes. WebSQL-only for now.
    storeName   : 'builds', // Should be alphanumeric, with underscores.
    description : 'stored builds'
});

var config = {};

config.urlprepend = '/app_dev.php';

config.cloudsupport = true;