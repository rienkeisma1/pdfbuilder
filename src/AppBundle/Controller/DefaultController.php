<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('app/default/index.html.twig', [

        ]);
    }

    /**
     * @Route("/edit/{projectid}", name="editpage")
     * @Method({"GET"})
     */
    public function editAction(Request $request, $projectid)
    {
        // replace this example code with whatever you need
        return $this->render('app/default/edit.html.twig', [
            'projectid' => $projectid,
            'demo' => 'false'
        ]);

    }

    /**
     * @Route("/create", name="createpage")
     * @Method({"GET"})
     */
    public function createAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('app/default/create.html.twig', [
            'demo' => false,    
        ]);

    }

    /**
     * @Route("/list", name="listpage")
     * @Method({"GET"})
     */
    public function listAction(Request $request)
    {

        return $this->render('app/default/list.html.twig', [
           
        ]);

    }

    /**
     * @Route("/demo", name="demopage")
     * @Method({"GET"})
     */
    public function demoAction(Request $request)
    {
    
        return $this->render('app/default/create.html.twig', [
            'demo' => true,
        ]);

    }

    /**
     * @Route("/loadbackup", name="loadbackuppage")
     * @Method({"GET"})
     */
    public function loadbackuppageAction(Request $request)
    {
    
        return $this->render('app/default/loadbackup.html.twig', [
            
        ]);

    }

   

    
}
