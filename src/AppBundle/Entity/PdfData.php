<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PdfData
 *
 * @ORM\Table(name="pdf_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PdfDataRepository")
 */
class PdfData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="border", type="string", length=20, nullable=true)
     */
    private $border;

    /**
     * @var string
     *
     * @ORM\Column(name="head", type="text", nullable=true)
     */
    private $head;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="javascript", type="text", nullable=true)
     */
    private $javascript;

    /**
     * @var string
     *
     * @ORM\Column(name="css", type="text", nullable=true)
     */
    private $css;

    /**
     * @var string
     *
     * @ORM\Column(name="header", type="text", nullable=true)
     */
    private $header;

    /**
     * @var string
     *
     * @ORM\Column(name="footer", type="text", nullable=true)
     */
    private $footer;

    /**
     * @var bool
     *
     * @ORM\Column(name="showheader", type="boolean")
     */
    private $showheader;

    /**
     * @var string
     *
     * @ORM\Column(name="headerheight", type="string", length=20, nullable=true)
     */
    private $headerheight;

    /**
     * @var bool
     *
     * @ORM\Column(name="showfooter", type="boolean")
     */
    private $showfooter;

    /**
     * @var string
     *
     * @ORM\Column(name="footerheight", type="string", length=20, nullable=true)
     */
    private $footerheight;

    /**
     * @var string
     *
     * @ORM\Column(name="created", type="string", length=20, nullable=true)
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="lastupdate", type="string", length=20, nullable=true)
     */
    private $lastupdate;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean" , nullable=true)
     */
    private $deleted;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="pdfuser")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */


    private $userpdfs;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

     /**
     * Set name
     *
     * @param string $name
     *
     * @return PdfData
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set border
     *
     * @param string $border
     *
     * @return PdfData
     */
    public function setBorder($border)
    {
        $this->border = $border;

        return $this;
    }

    /**
     * Get border
     *
     * @return string
     */
    public function getBorder()
    {
        return $this->border;
    }

    /**
     * Set head
     *
     * @param string $head
     *
     * @return PdfData
     */
    public function setHead($head)
    {
        $this->head = $head;

        return $this;
    }

    /**
     * Get head
     *
     * @return string
     */
    public function getHead()
    {
        return $this->head;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return PdfData
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set javascript
     *
     * @param string $javascript
     *
     * @return PdfData
     */
    public function setJavascript($javascript)
    {
        $this->javascript = $javascript;

        return $this;
    }

    /**
     * Get javascript
     *
     * @return string
     */
    public function getJavascript()
    {
        return $this->javascript;
    }

    /**
     * Set css
     *
     * @param string $css
     *
     * @return PdfData
     */
    public function setCss($css)
    {
        $this->css = $css;

        return $this;
    }

    /**
     * Get css
     *
     * @return string
     */
    public function getCss()
    {
        return $this->css;
    }

    /**
     * Set header
     *
     * @param string $header
     *
     * @return PdfData
     */
    public function setHeader($header)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * Get header
     *
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set footer
     *
     * @param string $footer
     *
     * @return PdfData
     */
    public function setFooter($footer)
    {
        $this->footer = $footer;

        return $this;
    }

    /**
     * Get footer
     *
     * @return string
     */
    public function getFooter()
    {
        return $this->footer;
    }

    /**
     * Set showheader
     *
     * @param boolean $showheader
     *
     * @return PdfData
     */
    public function setShowheader($showheader)
    {
        $this->showheader = $showheader;

        return $this;
    }

    /**
     * Get showheader
     *
     * @return bool
     */
    public function getShowheader()
    {
        return $this->showheader;
    }

    /**
     * Set headerheight
     *
     * @param string $headerheight
     *
     * @return PdfData
     */
    public function setHeaderheight($headerheight)
    {
        $this->headerheight = $headerheight;

        return $this;
    }

    /**
     * Get headerheight
     *
     * @return string
     */
    public function getHeaderheight()
    {
        return $this->headerheight;
    }

    /**
     * Set showfooter
     *
     * @param boolean $showfooter
     *
     * @return PdfData
     */
    public function setShowfooter($showfooter)
    {
        $this->showfooter = $showfooter;

        return $this;
    }

    /**
     * Get showfooter
     *
     * @return bool
     */
    public function getShowfooter()
    {
        return $this->showfooter;
    }

    /**
     * Set footerheight
     *
     * @param string $footerheight
     *
     * @return PdfData
     */
    public function setFooterheight($footerheight)
    {
        $this->footerheight = $footerheight;

        return $this;
    }

    /**
     * Get footerheight
     *
     * @return string
     */
    public function getFooterheight()
    {
        return $this->footerheight;
    }

    /**
     * Set pdfuser
     *
     * @param \AppBundle\Entity\User $pdfuser
     *
     * @return PdfData
     */
    public function setPdfuser(\AppBundle\Entity\User $pdfuser = null)
    {
        $this->pdfuser = $pdfuser;

        return $this;
    }

    /**
     * Get pdfuser
     *
     * @return \AppBundle\Entity\User
     */
    public function getPdfuser()
    {
        return $this->pdfuser;
    }


    /**
     * Set created
     *
     * @param integer $created
     *
     * @return PdfData
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return integer
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set lastupdate
     *
     * @param integer $lastupdate
     *
     * @return PdfData
     */
    public function setLastupdate($lastupdate)
    {
        $this->lastupdate = $lastupdate;

        return $this;
    }

    /**
     * Get lastupdate
     *
     * @return integer
     */
    public function getLastupdate()
    {
        return $this->lastupdate;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return PdfData
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

   

    /**
     * Set userpdfs
     *
     * @param \AppBundle\Entity\User $userpdfs
     *
     * @return PdfData
     */
    public function setUserpdfs(\AppBundle\Entity\User $userpdfs = null)
    {
        $this->userpdfs = $userpdfs;

        return $this;
    }

    /**
     * Get userpdfs
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserpdfs()
    {
        return $this->userpdfs;
    }
}
