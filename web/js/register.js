var formregister = {};

$(function() {

	formregister.addEventHandlers();
	
});

formregister.addEventHandlers = function(){

	$('form#registerform').on('submit', function(e){

		e.preventDefault();

		$('#loader').css('display', 'block');

		auth.register($('input#registerEmail').val(), $('input#registerPassword').val(), function(res){

			$('#loader').css('display', 'none');

			if(typeof res.msg !== 'undefined'){

				$('#msgbox').empty().html(res.msg);

				$('form#registerform').hide();
				$('form#loginform').hide();

				$('div#logoutholder').show();

				//loggedin now call get listdata

				listhandler.handleCloud();

			}else if(typeof res.err !== 'undefined'){

				$('#msgbox').html('an error occurred: '+JSON.stringify(res.err));

			}else{

				$('#msgbox').html('unknown response: '+JSON.stringify(err));
			}

		}, function(err){

			$('#msgbox').empty().html('an error occurred: '+JSON.stringify(res.err));

		});

	})

}