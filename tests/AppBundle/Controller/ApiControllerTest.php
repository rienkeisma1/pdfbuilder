<?php

namespace Tests\AppBundle\Controller;

use Doctrine\ORM\Tools\SchemaTool;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class ApiControllerTest extends WebTestCase
{

	private $token;

	private $refresh_token;

	private $user;

	private $client;

	private $pdfcreated;

	private $pdfcreatedts;

	private $pdfcreatedupdate;

	protected function setUp()
    {

    	$em = $this->getContainer()->get('doctrine')->getManager();
        if (!isset($metadatas)) {
            $metadatas = $em->getMetadataFactory()->getAllMetadata();
        }
        $schemaTool = new SchemaTool($em);
        $schemaTool->dropDatabase();
        if (!empty($metadatas)) {
            $schemaTool->createSchema($metadatas);
        }
        $this->postFixtureSetup();

        $fixtures = array(
           
        );
        $this->loadFixtures($fixtures);

        $this->client = static::createClient();
       
    }


    //test rejection
    public function testLogincheck()
    {

        $crawler = $this->client->request('POST', '/api/login_check');

        $this->assertTrue(
		    $this->client->getResponse()->headers->contains(
		        'Content-Type',
		        'application/json'
		    ),
		    'the "Content-Type" header is not "application/json"' // optional message shown on failure
		);

		$this->assertEquals(401, $this->client->getResponse()->getStatusCode());

    }

    //test register
    public function testRegister()
    {

        $this->registerTest();

        //try logincheck

        $crawler = $this->client->request('POST', '/api/login_check', [], [], ['HTTP_Authorization' => 'Bearer ' . $this->token]);

        $this->assertTrue(
		    $this->client->getResponse()->headers->contains(
		        'Content-Type',
		        'application/json'
		    ),
		    'the "Content-Type" header is not "application/json"' // optional message shown on failure
		);

        
		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());

		$res = $this->client->getResponse()->getContent();

		$json = json_decode($res, true);

        //get json error
        if($json === null){
	
        	$this->assertEquals($json, 'something', 'json not valid: '. json_last_error_msg());

        }

        $this->assertArrayHasKey('msg', $json);

        $this->assertTrue($json['msg'] === 'success' );
        
        //test logincheck
        $this->LogincheckLoggedIn();

        //logout
        $this->LogOut();

        //test logout
        $this->LogincheckLoggedOut();

        //login
        $this->LogIn();

        //test login
        $this->LogincheckLoggedIn();

    }

    public function registerTest()
    {

    	$crawler = $this->client->request('POST', '/api/register', ['email' => 'test@email.com', 'password' => 'testpassword']);

        // Assert that the "Content-Type" header is "application/json"
        
		$this->assertTrue(
		    $this->client->getResponse()->headers->contains(
		        'Content-Type',
		        'application/json'
		    ),
		    'the "Content-Type" header is not "application/json"' // optional message shown on failure
		);

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $res = $this->client->getResponse()->getContent();

        //uncomment to test if test catches malformed json
        //$res = substr($res,60);

        $json = json_decode($res, true);

        //get json error
        if($json === null){
	
        	$this->assertEquals($json, 'something', 'json not valid: '. json_last_error_msg());

        }
        
        //test if response contains needed keys and values are not null or empty string
        $this->assertArrayHasKey('token', $json);
        $this->assertArrayHasKey('refresh_token', $json);
        $this->assertArrayHasKey('user', $json);

        $this->token = $json['token'];
        $this->refresh_token = $json['refresh_token'];
        $this->user = $json['user'];

        if(isset($json['token'])){

        	$this->assertTrue(count($json['token']) !== 0 );

        }

        if(isset($json['refresh_token'])){

        	$this->assertTrue(count($json['refresh_token']) !== 0 );
        	
        }

        if(isset($json['user'])){

        	$this->assertTrue(count($json['user']) !== 0 );
        	
        }

    }

    //test success
    public function LogincheckLoggedIn()
    {

        $crawler = $this->client->request('POST', '/api/login_check', [], [], ['HTTP_Authorization' => 'Bearer ' . $this->token]);

        $this->assertTrue(
		    $this->client->getResponse()->headers->contains(
		        'Content-Type',
		        'application/json'
		    ),
		    'the "Content-Type" header is not "application/json"' // optional message shown on failure
		);

        
		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());

		$res = $this->client->getResponse()->getContent();

		$json = json_decode($res, true);

        //get json error
        if($json === null){
	
        	$this->assertEquals($json, 'something', 'json not valid: '. json_last_error_msg());

        }

        $this->assertArrayHasKey('msg', $json);

        $this->assertTrue($json['msg'] === 'success' );

    }

    //test logout
    public function LogOut()
    {

        //just set client token to empty string
        $this->token = '';

    }

    //test login
    public function LogIn()
    {

        //login
       
        $crawler = $this->client->request('POST', '/api/login', ['email' => 'test@email.com', 'password' => 'testpassword']);

        // Assert that the "Content-Type" header is "application/json"
        
		$this->assertTrue(
		    $this->client->getResponse()->headers->contains(
		        'Content-Type',
		        'application/json'
		    ),
		    'the "Content-Type" header is not "application/json"' // optional message shown on failure
		);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $res = $this->client->getResponse()->getContent();

        //uncomment to test if test catches malformed json
        //$res = substr($res,60);

        $json = json_decode($res, true);

        //get json error
        if($json === null){
	
        	$this->assertEquals($json, 'something', 'json not valid: '. json_last_error_msg());

        }
        
        //test if response contains needed keys and values are not null or empty string
        $this->assertArrayHasKey('token', $json);
        $this->assertArrayHasKey('refresh_token', $json);
        $this->assertArrayHasKey('user', $json);

        $this->token = $json['token'];
        $this->refresh_token = $json['refresh_token'];
        $this->user = $json['user'];

        if(isset($json['token'])){

        	$this->assertTrue(count($json['token']) !== 0 );

        }

        if(isset($json['refresh_token'])){

        	$this->assertTrue(count($json['refresh_token']) !== 0 );
        	
        }

        if(isset($json['user'])){

        	$this->assertTrue(count($json['user']) !== 0 );
        	
        }

    }

     //test loggedout
    public function LogincheckLoggedOut()
    {

        $crawler = $this->client->request('POST', '/api/login_check');

        $this->assertTrue(
		    $this->client->getResponse()->headers->contains(
		        'Content-Type',
		        'application/json'
		    ),
		    'the "Content-Type" header is not "application/json"' // optional message shown on failure
		);

		$this->assertEquals(401, $this->client->getResponse()->getStatusCode());

    }


    /**
     * pdf storage create update and delete tests
     */

    public function testCreatePdfDataAction()
    {

    	//first register
    	$this->registerTest();

    	//create pdfdata
    	$this->createPdfData();

    	//get pdf
    	$this->getSinglePdfData();

    	//updatepdf
    	$this->updatePdfData();

    	//check if updated
    	$this->getSingleUpdated();

    	//delete
    	$this->deletePdf();

    	//checkdeleted
    	$this->getSingleDeletedPdf();

    	//try to create deleted should return 409
    	$this->createDeletedPdfData();

    	//try to create update should return 409
    	$this->updateDeletedPdfData();

    }

    public function createDeletedPdfData()
    {

    	//test data
    	$input = $this->createPdfDataHelper();

    	$crawler = $this->client->request('POST', '/api/createPdfData', $input, [], ['HTTP_Authorization' => 'Bearer ' . $this->token]);

        $this->assertTrue(
		    $this->client->getResponse()->headers->contains(
		        'Content-Type',
		        'application/json'
		    ),
		    'the "Content-Type" header is not "application/json"'
		);

		$this->assertEquals(409, $this->client->getResponse()->getStatusCode());

    }

    public function deletePdf(){

    	$crawler = $this->client->request('POST', '/api/deletePdf', ['created' => $this->pdfcreatedts], [], ['HTTP_Authorization' => 'Bearer ' . $this->token]);

    	dump($this->client->getResponse()->getStatusCode());

        $this->assertTrue(
		    $this->client->getResponse()->headers->contains(
		        'Content-Type',
		        'application/json'
		    ),
		    'the "Content-Type" header is not "application/json"' // optional message shown on failure
		);

		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());


    }

    public function getSingleUpdated()
    {

    	$crawler = $this->client->request('GET', '/api/getPdf/' . $this->pdfcreatedts, [], [], ['HTTP_Authorization' => 'Bearer ' . $this->token]);

        $this->assertTrue(
		    $this->client->getResponse()->headers->contains(
		        'Content-Type',
		        'application/json'
		    ),
		    'the "Content-Type" header is not "application/json"' // optional message shown on failure
		);

		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());

		$res = $this->client->getResponse()->getContent();

		$res = json_decode($res, true);

		//multidimensional array compare sorted json encoded strings
		$resdata = $res['data'];
		array_multisort($resdata);
		$resdata['header'] = sort($resdata['header']);
		$resdata['footer'] = sort($resdata['footer']);

		$compare = $this->pdfcreated;
		array_multisort($compare);
		$compare['header'] = sort($compare['header']);
		$compare['footer'] = sort($compare['footer']);

		$compareupdated = $this->pdfcreatedupdate;
		array_multisort($compareupdated);
		$compareupdated['header'] = sort($compareupdated['header']);
		$compareupdated['footer'] = sort($compareupdated['footer']);


		$resdata = json_encode($resdata);
		$compare = json_encode($compare);
		$compareupdated = json_encode($compareupdated);

		$this->assertTrue($resdata !== $compare);
		$this->assertTrue($resdata === $compareupdated);

    }

    public function updatePdfData()
    {

    	$input = $this->createUpdatedPdfDataHelper();

    	$crawler = $this->client->request('POST', '/api/updatePdfData', $input, [], ['HTTP_Authorization' => 'Bearer ' . $this->token]);

        $this->assertTrue(
		    $this->client->getResponse()->headers->contains(
		        'Content-Type',
		        'application/json'
		    ),
		    'the "Content-Type" header is not "application/json"' // optional message shown on failure
		);

		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());

    }

    public function updateDeletedPdfData()
    {

    	$input = $this->createUpdatedPdfDataHelper();

    	$crawler = $this->client->request('POST', '/api/updatePdfData', $input, [], ['HTTP_Authorization' => 'Bearer ' . $this->token]);

        $this->assertTrue(
		    $this->client->getResponse()->headers->contains(
		        'Content-Type',
		        'application/json'
		    ),
		    'the "Content-Type" header is not "application/json"' // optional message shown on failure
		);

		$this->assertEquals(409, $this->client->getResponse()->getStatusCode());

    }

    public function createPdfData()
    {

    	//test data
    	$input = $this->createPdfDataHelper();

    	$crawler = $this->client->request('POST', '/api/createPdfData', $input, [], ['HTTP_Authorization' => 'Bearer ' . $this->token]);



        $this->assertTrue(
		    $this->client->getResponse()->headers->contains(
		        'Content-Type',
		        'application/json'
		    ),
		    'the "Content-Type" header is not "application/json"' // optional message shown on failure
		);

		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());

    }

    public function getSinglePdfData(){

    	$crawler = $this->client->request('GET', '/api/getPdf/' . $this->pdfcreatedts, [], [], ['HTTP_Authorization' => 'Bearer ' . $this->token]);

        $this->assertTrue(
		    $this->client->getResponse()->headers->contains(
		        'Content-Type',
		        'application/json'
		    ),
		    'the "Content-Type" header is not "application/json"' // optional message shown on failure
		);

		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());

		$res = $this->client->getResponse()->getContent();

		$res = json_decode($res, true);

		//multidimensional array compare sorted json encoded strings

		$resdata = $res['data'];
		array_multisort($resdata);
		$resdata['header'] = sort($resdata['header']);
		$resdata['footer'] = sort($resdata['footer']);

		$compare = $this->pdfcreated;
		array_multisort($compare);
		$compare['header'] = sort($compare['header']);
		$compare['footer'] = sort($compare['footer']);


		$resdata = json_encode($resdata);
		$compare = json_encode($compare);

		$this->assertTrue($resdata === $compare);



    }

    public function getSingleDeletedPdf(){

    	$crawler = $this->client->request('GET', '/api/getPdf/' . $this->pdfcreatedts, [], [], ['HTTP_Authorization' => 'Bearer ' . $this->token]);

        $this->assertTrue(
		    $this->client->getResponse()->headers->contains(
		        'Content-Type',
		        'application/json'
		    ),
		    'the "Content-Type" header is not "application/json"' // optional message shown on failure
		);

		$this->assertEquals(404, $this->client->getResponse()->getStatusCode());

    }

    public function createUpdatedPdfDataHelper()
    {

    	$date = new \DateTime();
		$ts = $date->getTimestamp() * 1000;

        $input = $this->createPdfDataHelper();

        $input['lastupdate'] = (string)$ts;
        $input['name'] = 'testpdf2';
        $input['created'] = (string)$this->pdfcreatedts;

        $this->pdfcreatedupdate = $input;

        return $input;

    }

    public function createPdfDataHelper()
    {

    	$date = new \DateTime();
		$ts = $date->getTimestamp() * 1000;
    	$input['name'] = 'testpdf';
        $input['border'] = '0px';
        $input['headval'] = '<meta charset="utf8"/>';
        $input['bodyval'] = '<h1>body value</h1>';
        $input['jsval'] = 'var x = 1';
        $input['cssval'] = 'h1 {color: blue}';
        $input['header']['html'] = '<h1>header</h1>';
        $input['footer']['html'] = '<h1>footer</h1>';
        $input['header']['showheader'] = true;
        $input['header']['height'] = '30px';
        $input['footer']['showfooter'] = true;
        $input['footer']['height'] = '50px';
        $input['lastupdate'] = (string)$ts;
        $input['created'] = (string)$ts;

        $this->pdfcreatedts = $ts;

        $this->pdfcreated = $input;

        return $input;

    }

}
