var crud = {};

crud.createProject = function(created, callback, errorcallback) {

	localforage.getItem('builds').then(function(res) {

		let project = {};

		for(let i = 0; i < res.length; i++){

			if(res[i].created === created) {

				project = res[i];
				hit = true;
				break;

			}

		}

		if(hit === false){

			errorcallback({err: 'this project cannot be found'});

		}else{

			auth.call(baseurl + config.urlprepend+'/api/createPdfData', project, 'POST', function(res){

				callback({msg: 'created project: '+project.name});

			});

		}

	}).catch(function(err){

		errorcallback({err: 'an error occurred: '+JSON.stringify(err)});

	})

}

crud.updateServer = function(callback){
	//get localdata here to keep it simple (not optimal because it may be available in the calling function)

	localforage.getItem('builds').then(function(res){

		return auth.call(baseurl + config.urlprepend+'/api/updatePdfData', {data:res}, 'POST', callback);

	}).catch(function(err){

		$('#msgbox').empty().html('error: '+JSON.stringify(err));

	})

}

crud.getProjectsMetaData = function(callback) {

	return auth.call(baseurl + config.urlprepend+'/api/getPdfsMetaData', {}, 'GET', callback);

}

crud.deleteProject = function(created, callback) {

	return auth.call(baseurl + config.urlprepend+'/api/deletePdf', {created: created}, 'POST', callback);

}

crud.getProject = function(created, callback, error) {

	return auth.call(baseurl + config.urlprepend+'/api/getPdf/'+created, {}, 'GET', callback, error);

}

crud.updateProject = function(created, callback, error) {

	localforage.getItem('builds').then(function(res) {

		let project = {};

		for(let i = 0; i < res.length; i++){

			if(res[i].created === created) {

				project = res[i];
				hit = true;
				break;

			}

		}

		if(hit === false){

			errorcallback({err: 'this project cannot be found'});

		}else{

			auth.call(baseurl + config.urlprepend+'/api/updatePdfData', project, 'POST', function(res){

				callback({msg: 'updated project: '+project.name});

			});

		}

	}).catch(function(err){

		errorcallback({err: 'an error occurred: '+JSON.stringify(err)});

	})

}
